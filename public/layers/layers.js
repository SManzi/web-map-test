var wms_layers = [];

        var lyr_OpenStreetMap_0 = new ol.layer.Tile({
            'title': 'OpenStreetMap',
            'type': 'base',
            'opacity': 1.000000,
            
            
            source: new ol.source.XYZ({
    attributions: '<a href=""></a>',
                url: 'http://a.tile.openstreetmap.org/{z}/{x}/{y}.png'
            })
        });var format_testInputv2_1 = new ol.format.GeoJSON();
var features_testInputv2_1 = format_testInputv2_1.readFeatures(json_testInputv2_1, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_testInputv2_1 = new ol.source.Vector({
    attributions: '<a href=""></a>',
});
jsonSource_testInputv2_1.addFeatures(features_testInputv2_1);var lyr_testInputv2_1 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_testInputv2_1, 
                style: style_testInputv2_1,
                title: '<img src="styles/legend/testInputv2_1.png" /> testInputv2'
            });

lyr_OpenStreetMap_0.setVisible(true);lyr_testInputv2_1.setVisible(true);
var layersList = [lyr_OpenStreetMap_0,lyr_testInputv2_1];
lyr_testInputv2_1.set('fieldAliases', {'Area': 'Area', 'Easting': 'Easting', 'Northing': 'Northing', 'PWP patients': 'PWP patients', 'PWP average': 'PWP average', 'PWP max': 'PWP max', 'CBT patients': 'CBT patients', 'CBT average': 'CBT average', 'CBT max': 'CBT max', 'PT_CT patients': 'PT_CT patients', 'PT_CT average': 'PT_CT average', 'PT_CT max': 'PT_CT max', });
lyr_testInputv2_1.set('fieldImages', {'Area': 'TextEdit', 'Easting': 'Range', 'Northing': 'Range', 'PWP patients': 'Range', 'PWP average': 'Range', 'PWP max': 'Range', 'CBT patients': 'Range', 'CBT average': 'Range', 'CBT max': 'Range', 'PT_CT patients': 'Range', 'PT_CT average': 'Range', 'PT_CT max': 'Range', });
lyr_testInputv2_1.set('fieldLabels', {'Area': 'header label', 'Easting': 'no label', 'Northing': 'no label', 'PWP patients': 'inline label', 'PWP average': 'inline label', 'PWP max': 'inline label', 'CBT patients': 'inline label', 'CBT average': 'inline label', 'CBT max': 'inline label', 'PT_CT patients': 'inline label', 'PT_CT average': 'inline label', 'PT_CT max': 'inline label', });
lyr_testInputv2_1.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});