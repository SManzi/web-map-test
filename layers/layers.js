var wms_layers = [];

        var lyr_OpenStreetMap_0 = new ol.layer.Tile({
            'title': 'Open Street Map',
            'type': 'base',
            'opacity': 1.000000,
            
            
            source: new ol.source.XYZ({
    attributions: '<a href=""></a>',
                url: 'https://a.tile.openstreetmap.org/{z}/{x}/{y}.png'
            })
        });var format_exampleInput_1 = new ol.format.GeoJSON();
var features_exampleInput_1 = format_exampleInput_1.readFeatures(json_exampleInput_1, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_exampleInput_1 = new ol.source.Vector({
    attributions: '<a href=""></a>',
});
jsonSource_exampleInput_1.addFeatures(features_exampleInput_1);var lyr_exampleInput_1 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_exampleInput_1, 
                style: style_exampleInput_1,
                title: '<img src="styles/legend/exampleInput_1.png" /> exampleInput'
            });

lyr_OpenStreetMap_0.setVisible(true);lyr_exampleInput_1.setVisible(true);
var layersList = [lyr_OpenStreetMap_0,lyr_exampleInput_1];
lyr_exampleInput_1.set('fieldAliases', {'Area': 'Location', 'Easting': 'Easting', 'Northing': 'Northing', 'PWP patients': 'PWP patients', 'PWP average': 'PWP average', 'PWP max': 'PWP max', 'CBT patients': 'CBT patients', 'CBT average': 'CBT average', 'CBT max': 'CBT max', 'PT_CT patients': 'PT_CT patients', 'PT_CT average': 'PT_CT average', 'PT_CT max': 'PT_CT max', });
lyr_exampleInput_1.set('fieldImages', {'Area': 'TextEdit', 'Easting': 'Hidden', 'Northing': 'Hidden', 'PWP patients': 'Range', 'PWP average': 'Range', 'PWP max': 'Range', 'CBT patients': 'Range', 'CBT average': 'Range', 'CBT max': 'Range', 'PT_CT patients': 'Range', 'PT_CT average': 'Range', 'PT_CT max': 'Range', });
lyr_exampleInput_1.set('fieldLabels', {'Area': 'header label', 'PWP patients': 'inline label', 'PWP average': 'inline label', 'PWP max': 'inline label', 'CBT patients': 'inline label', 'CBT average': 'inline label', 'CBT max': 'inline label', 'PT_CT patients': 'inline label', 'PT_CT average': 'inline label', 'PT_CT max': 'inline label', });
lyr_exampleInput_1.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});